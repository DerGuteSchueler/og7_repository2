package controller;

import model.*;
import java.util.List;
import java.util.LinkedList;

public class BunteRechteckeController {

	public static void main(String[] args) {
		
	}

	private List<Rechteck> rechtecke;

	public BunteRechteckeController() {
		super();
		this.rechtecke = new LinkedList();
	}

	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
	}
	
	public void reset() {
		rechtecke.clear();
	}

	public List<Rechteck> getRechtecke() {
		return rechtecke;
	}

	public void setRechtecke(List rechtecke) {
		this.rechtecke = rechtecke;
	}
	
	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		this.reset();
		for (int i = 0; i < anzahl ; i++) {
			rechtecke.add(Rechteck.generiereZufallsRechteck());
		}
	}
	
}
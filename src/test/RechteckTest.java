package test;

import controller.BunteRechteckeController;
import model.*;

public class RechteckTest {

	public static void main(String[] args) {
		Rechteck r0 = new Rechteck();
		r0.setX(10);
		r0.setY(10);
		r0.setBreite(30);
		r0.setHoehe(40);
		Rechteck r1 = new Rechteck();
		r1.setX(25);
		r1.setY(25);
		r1.setBreite(100);
		r1.setHoehe(20);
		Rechteck r2 = new Rechteck();
		r2.setX(260);
		r2.setY(10);
		r2.setBreite(200);
		r2.setHoehe(100);
		Rechteck r3 = new Rechteck();
		r3.setX(5);
		r3.setY(500);
		r3.setBreite(300);
		r3.setHoehe(25);
		Rechteck r4 = new Rechteck();
		r4.setX(100);
		r4.setY(100);
		r4.setBreite(100);
		r4.setHoehe(100);
		Rechteck r5 = new Rechteck(200,200,200,200);
		Rechteck r6 = new Rechteck(800,400,20,20);
		Rechteck r7 = new Rechteck(800,450,20,20);
		Rechteck r8 = new Rechteck(850,400,20,20);
		Rechteck r9 = new Rechteck(855,455,25,25);
		System.out.println(r0.toString());
		System.out.println("------------");
		
		BunteRechteckeController controllerobjekt1 = new BunteRechteckeController();
		controllerobjekt1.add(r0);
		controllerobjekt1.add(r1);
		controllerobjekt1.add(r2);
		controllerobjekt1.add(r3);
		controllerobjekt1.add(r4);
		controllerobjekt1.add(r5);
		controllerobjekt1.add(r6);
		controllerobjekt1.add(r7);
		controllerobjekt1.add(r8);
		controllerobjekt1.add(r9);
		
		System.out.println(controllerobjekt1.toString());
		System.out.println("------------");
		Rechteck r10 = new Rechteck(-4,-5,-50,-200);
		System.out.println(r10);
		
		Rechteck r11 = new Rechteck();
		r11.setX(-10);
		r11.setY(-10);
		r11.setBreite(-200);
		r11.setHoehe(-100);
		System.out.println(r11);
		System.out.println("------------");
		Rechteck r12 = new Rechteck(400,400,100,100);
		System.out.println(r12.enthaelt(500, 500));
		
		System.out.println("------------");
		Rechteck r13 = new Rechteck(400,400,100,100);
		Rechteck r14 = new Rechteck(400,400,100,100);
		System.out.println(r13.enthaelt(r14));
		
		System.out.println("------------");
		Rechteck r15 = Rechteck.generiereZufallsRechteck();
		System.out.println(r15);
		
		System.out.println("------------");
		rechteckeTesten();
	}

	public static void rechteckeTesten() {
		boolean wahrheit = false;
		Rechteck rAbgleich = new Rechteck(0, 0, 1200, 1000);
		Rechteck[] rechtecke = new Rechteck[50000];
		for (int i = 0; i < rechtecke.length; i++) {
			rechtecke[i]= Rechteck.generiereZufallsRechteck();
			if (rAbgleich.enthaelt(rechtecke[i])) {
				wahrheit = true;
			}
		}
		System.out.println("Alle Rechtecke in den vorgegebenen Parameter: " + wahrheit);
	}
}
